﻿using System;
using System.IO;

namespace TVMoviesRename
{
    class Program
    {
        static void Main()
        {
            Console.Write(@"Are we moving 'movies' or 'tv'?" + '\n');
            string type = Console.ReadLine();

            // Prompt for the TV Series Directory
            Console.Write(@"Please insert the location of the Directory" + '\n' + @"Example: D:\Videos\TV Series\Oldies" + '\n' + @"Example: D:\Videos\Movies\Comedies" + '\n');
            string path = Console.ReadLine();

            if (type == @"tv")
            {
                moveTV(path);
            }
            else if (type == @"movies")
            {
                moveMovies(path);
            }

            Console.Write(@"Hit Enter to quit");
            Console.ReadLine();
        }

        static void moveTV(string path)
        {
            foreach (string series in Directory.GetDirectories(path))
            {
                foreach (string season in Directory.GetDirectories(series))
                {
                    foreach (string file in Directory.GetFiles(season))
                    {
                        string ext = Path.GetExtension(file);
                        if (Utils.isVideo(ext) || Utils.isNFO(ext) || Utils.isSubtitle(ext) || Utils.isThumbnail(ext))
                        {
                            string episode = Utils.getEpisodeNumber(Path.GetFileNameWithoutExtension(file));
                            // check if the episode number came back
                            if (episode != null && episode != String.Empty)
                            {
                                string seriesName = Utils.shortenSeriesName(Path.GetFileName(series).ToLower());
                                string seasonNumber = Path.GetFileName(season).ToLower();
                                if (seasonNumber == "specials")
                                {
                                    seasonNumber = "00";
                                }
                                else
                                {
                                    seasonNumber = seasonNumber.Replace("season ", "");
                                }
                                string newFilePath = Path.GetDirectoryName(file) + Path.DirectorySeparatorChar + seriesName + "_s" + seasonNumber + "e" + episode;
                                if (Utils.isThumbnail(ext))
                                {
                                    newFilePath = newFilePath + "-thumb";
                                }
                                newFilePath = newFilePath + Path.GetExtension(file);
                                Utils.moveFile(file, newFilePath);
                            }
                            else
                            {
                                Console.Write(@"No episode number found in " + file);
                            }
                        }
                    }
                }
            }
        }

        static void moveMovies(string path)
        {
            foreach (string movieFolder in Directory.GetDirectories(path))
            {
                int videoCount = Utils.countVideos(movieFolder);
                if (videoCount > 1)
                {
                    Console.Write("Too many videos in " + movieFolder + '\n');
                }
                else if (videoCount == 0)
                {
                    Console.Write("No videos in " + movieFolder + '\n');
                }
                else
                {
                    foreach (string video in Directory.GetFiles(movieFolder))
                    {
                        if (Utils.isVideo(Path.GetExtension(video)))
                        {
                            string newFilePath = movieFolder + Path.DirectorySeparatorChar + Path.GetFileName(movieFolder) + Path.GetExtension(video);
                            Utils.moveFile(video, newFilePath);
                        }
                    }
                }
            }
        }
    }
}
