﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace TVMoviesRename
{
    public class Utils
    {
        public static int countVideos(string path)
        {
            int count = 0;
            foreach (string file in Directory.GetFiles(path))
            {
                if (isVideo(Path.GetExtension(file)) == true)
                {
                    count++;
                }
            }
            return count;
        }

        public static bool isThumbnail(string ext)
        {
            return (ext.ToLower().Equals(@".jpg") == true);
        }

        public static bool isSubtitle(string ext)
        {
            return (ext.ToLower().Equals(@".srt") == true ||
                ext.ToLower().Equals(@".idx") == true);
        }

        public static bool isNFO(string ext)
        {
            return (ext.ToLower().Equals(@".nfo") == true);
        }

        public static bool isVideo(string ext)
        {
            return (ext.ToLower().Equals(@".avi") == true ||
                ext.ToLower().Equals(@".vob") == true ||
                ext.ToLower().Equals(@".mpg") == true ||
                ext.ToLower().Equals(@".mpeg") == true ||
                ext.ToLower().Equals(@".mkv") == true ||
                ext.ToLower().Equals(@".mov") == true ||
                ext.ToLower().Equals(@".wmv") == true ||
                ext.ToLower().Equals(@".mp4") == true ||
                ext.ToLower().Equals(@".m2v") == true ||
                ext.ToLower().Equals(@".m2p") == true ||
                ext.ToLower().Equals(@".m4v") == true ||
                ext.ToLower().Equals(@".3gp") == true ||
                ext.ToLower().Equals(@".ts") == true ||
                ext.ToLower().Equals(@".dvr-ms") == true ||
                ext.ToLower().Equals(@".dat") == true ||
                ext.ToLower().Equals(@".divx") == true ||
                ext.ToLower().Equals(@".dvx") == true ||
                ext.ToLower().Equals(@".flv") == true);
        }

        public static bool isDirEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        public static void moveFile(string sourcePath, string destPath)
        {
            if (sourcePath == destPath)
            {
                return;
            }

            try
            {
                Directory.Move(sourcePath, destPath);
                Console.Write("Moved: " + sourcePath + "\n" + "   ------> " + destPath);
                string dirName = Path.GetDirectoryName(sourcePath);
                if (isDirEmpty(dirName)) {
                    try
                    {
                        Directory.Delete(dirName);
                    } catch
                    {
                        Console.WriteLine("Oops - Didn't Delete Folder " + dirName);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Oops - Didn't Move " + sourcePath);
            }
            Console.Write('\n');
        }

        public static string getEpisodeNumber(string file)
        {
            // First, remove the common number formats from the string
            string stripped = file.ToLower()
                .Replace("1080p", "")
                .Replace("1080i", "")
                .Replace("720p", "")
                .Replace("540p", "")
                .Replace("480p", "")
                .Replace("x264", "")
                .Replace("h264", "")
                .Replace("h.264", "")
                .Replace("x265", "")
                .Replace("h265", "")
                .Replace("h.265", "");
            // Create a Regular Expression to pull out the episode number
            string regexStr = @"^(.*\D)?(\d+)(e|x|\.)?(?<episode>\d\d)(\D.*)?$";
            Regex regex = new Regex(regexStr, RegexOptions.IgnoreCase);
            return regex.Match(stripped).Groups["episode"].Value;
        }

        const string startingChar = @"startingChar";
        public static string shortenSeriesName(string name)
        {
            string shortened = name.Replace(", A", "").Replace(", The", "");
            Regex parensRegex = new Regex(@"\s\(.*", RegexOptions.IgnoreCase);
            shortened = parensRegex.Replace(shortened, "");
            Regex regex = new Regex(@"\s(?<" + startingChar + @">\S)(?<restChars>\S*)", RegexOptions.IgnoreCase);
            return regex.Replace(shortened, new MatchEvaluator(replaceWS)).Replace(" ", "").ToLower();
        }

        public static string replaceWS(Match match)
        {
            return match.Groups[startingChar].Value.ToUpper();
        }
    }
}
